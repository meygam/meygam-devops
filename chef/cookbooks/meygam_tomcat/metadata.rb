name             'meygam_tomcat'
maintainer       'meygam'
maintainer_email 'saravana.kaumr@meygam.io'
license          'All rights reserved'
description      'Installs/Configures meygam_tomcat'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
